# oo2txt #

This script exports Ocean Optics ProcSpec bianry files to txt. It is tested with spectrometer firmware version USB4000 1.01.11.

You can run the script like this:

    ./oo2txt.py filenames

