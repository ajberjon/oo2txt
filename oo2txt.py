#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import zipfile
import xml.etree.ElementTree as ET
import sys
import datetime
import re


# returns a zipfile tree
########################
def ooTree(procSpecFileName):
    archive = zipfile.ZipFile(procSpecFileName, 'r')
    namel=archive.namelist()
    namel.remove('OOISignatures.xml')
    namel.remove('OOIVersion.txt')
    data=archive.read(namel[0])
    tree = ET.ElementTree(ET.fromstring(data.decode('utf-8','ignore')))

    return tree


# Main program
##############
def main():
    for arg in sys.argv[1:]:
        if zipfile.is_zipfile(arg)==False:
            sys.exit("Error: %s is not a ProcSpec file"%(arg))
        root = ooTree(arg).getroot()
        spectra=root.find('sourceSpectra').find('com.oceanoptics.omnidriver.spectra.OmniSpectrum')
        pixelValues=[float(child.text) for child in spectra.find('pixelValues')]
        channelWavelengths=[float(child.text) for child in spectra.find('channelWavelengths')]
        if len(pixelValues)!=len(channelWavelengths):
            sys.exit("Error: Wrong pixel number.")

        fout = open(re.sub(".ProcSpec$",".txt",arg), 'w')
        fout.write("# Raw file name: %s\n"%(re.sub(".*/","",arg)))
        fout.write(datetime.datetime.fromtimestamp(float(spectra.find('acquisitionTime').find('milliTime').text)/1000).strftime('# Date: %Y-%m-%d %H:%M:%S\n'))
        fout.write("# User: %s\n"%(spectra.find('userName').text))
        fout.write("# Spectrometer Serial Number: %s\n"%(spectra.find('spectrometerSerialNumber').text))
        fout.write("# Spectrometer Firmware Version: %s\n"%(spectra.find('spectrometerFirmwareVersion').text))
        fout.write("# Correct for electrical dark: %s\n"%("No" if spectra.find('correctForElectricalDark').text=='false' else "Yes"))
        fout.write("# Correct for non linearity: %s\n"%("No" if spectra.find('correctForNonLinearity').text=='false' else "Yes"))
        for i in range(len(pixelValues)):
            fout.write("%.3f\t%.3f\n"%(channelWavelengths[i],pixelValues[i]))
        fout.close()


if __name__ == '__main__':
  main()
